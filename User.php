<?php

/**
 * Class User
 */
class User
{
    /**
     * @var string
     */
    private string $username;

    /**
     * @var string
     */
    private string $name;

    /**
     * User constructor.
     * @param string $username
     * @param string $name
     */
    public function __construct(string $username, string $name)
    {
        $this->username = $username;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        /** @var TYPE_NAME $username */
        if (!empty($username)) {
            $this->username = $username;
        }
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }


    public function test(string $someValue)
    {
        $test = 10;
        $someArray = [];

        for ($i = 0; $i <= $test; $i++) {
            $someArray[] = $i;
        }

        return $someArray;
    }

    public function someMethod(string $someValue)
    {
        $test = 20;
        $someArray = [];

        for ($i = 0; $i <= $test; $i++) {
            $someArray[] = $i;
        }

        return $someArray;
    }
}